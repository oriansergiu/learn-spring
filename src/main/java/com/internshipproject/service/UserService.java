package com.internshipproject.service;

import com.internshipproject.exception.InvalidEmailAddress;
import com.internshipproject.exception.UserException;
import com.internshipproject.model.CustomUserDetails;
import com.internshipproject.model.Filter;
import com.internshipproject.model.Filtration;
import com.internshipproject.model.User;
import com.internshipproject.repository.FiltrationRepository;
import com.internshipproject.repository.UserRepository;
import com.internshipproject.utils.CustomSession;
import com.internshipproject.utils.SmtpMailSender;
import com.internshipproject.utils.UserUtil;
import java.nio.file.AccessDeniedException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javassist.tools.web.BadHttpRequest;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.exception.SQLGrammarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FiltrationRepository filtrationRepository;

    @Autowired
    private CustomSession<User> customSession;

    @Autowired
    private UserUtil userUtil;

    @Autowired
    private SmtpMailSender smtpMailSender;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    public User login(User user) throws AccessDeniedException {
        CustomUserDetails userDetails = customUserDetailsService.loadUserByUsername(user.getUsername());

        String userHashPassword = userUtil.hashPassword(user.getPassword());

        if (userDetails.getUser() != null && userDetails.getPassword().equals(userHashPassword)) {
            Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return user;
        } else {
            LOGGER.info("User not found!");
            throw new AccessDeniedException("Incorrect username or password.");
        }
    }

    public User registration(User user) throws AccessDeniedException {
        user.setPassword(userUtil.hashPassword(user.getPassword()));

        User newUser = userRepository.save(user);
        if (newUser != null) {
            try {
                smtpMailSender.send(user.getUsername(), "Confirmation mail", "hello");
            } catch (MessagingException e) {
                LOGGER.warn("Invalid recipient email address");
                throw new AccessDeniedException("Wrong email Address.");
            }
        }
        return user;
    }

    public void delete(Integer id) {
        //Delete user filtration
        String username = userRepository.findOne(id).getUsername();
        Integer filtrationId = filtrationRepository.findByUsername(username).getId();
        filtrationRepository.delete(filtrationId);

        userRepository.delete(id);
    }

    public User update(Integer id, User user) {

        String hashPassword = userUtil.hashPassword(user.getPassword());
        user.setPassword(hashPassword);

        user.setId(id);
        userRepository.save(user);
        return user;
    }

    public List<User> getAll() {

        UserDetails userDetails =
            (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Filtration filtration;
        filtration = filtrationRepository.findByUsername(userDetails.getUsername());

        if (filtration == null) {
            return (List<User>) userRepository.findAll();
        }

        return filterBy(filtration.getFilters());
    }

    public List<User> filterBy(List<Filter> filterParameters) {
        Map<String, List<String>> filters = new HashMap<>();
        List<User> users;

        for (Filter filter : filterParameters) {
            filters.put(filter.getFilterKey(), filter.getElements());
        }
        try {
            users = customSession.filtration(User.class, filters);
        } catch (SQLGrammarException ex) {
            throw new UserException("failed.filter", HttpStatus.BAD_REQUEST, "The filter parameters aren't good.");
        }
        return users;
    }

    public List<User> sorted(String category, String sortType) {
        List<User> users;

        try {
            users = customSession.sorting(User.class, category, sortType);
            return users;
        } catch (SQLGrammarException ex) {
            throw new UserException("failed.sorted", HttpStatus.BAD_REQUEST, "The sorted parameters aren't good.");
        }
    }
}