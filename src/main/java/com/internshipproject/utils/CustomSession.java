package com.internshipproject.utils;

import java.util.List;

import java.util.Map;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

@Component
public class CustomSession<T> {

    private static SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public List<T> sorting(Object clazz, String sortParam, String sortType) {
        String tableName = this.tableName(clazz);
        String query = this.createSortQuery(sortParam, sortType, tableName);

        return executeQuery(query);
    }

    private String createSortQuery(String sortParam, String sortType, String tableName) {
        String query = "from " + tableName + " ORDER BY " + sortParam;
        if (sortType != null) {
            query += " " + sortType;
        }
        return query;
    }

    public List<T> filtration(Object clazz, Map<String, List<String>> filters) {

        String tableName = this.tableName(clazz);
        String query = this.createFilterQuery(filters, tableName);

        return executeQuery(query);
    }

    /**
     * @return the entity name
     */
    private String tableName(Object clazz) {
        String[] entityRootFields = clazz.toString().split("\\.");
        return entityRootFields[entityRootFields.length - 1];
    }

    private String createFilterQuery(Map<String, List<String>> filters, String tableName) {
        String query = "from " + tableName + " where (";
        for (String key :
            filters.keySet()) {

            List<String> filtersValues = filters.get(key);
            for (String value : filtersValues) {
                query += key + " = '" + value + "' OR ";
            }
            query = query.substring(0, query.length() - 4);
            query += ") AND (";
        }
        return query.substring(0, query.length() - 6);
    }

    public List<T> executeQuery(String query) {

        Session session = sessionFactory.openSession();
        List<T> entities = null;

        Transaction transaction = session.beginTransaction();
        try {
            entities = session.createQuery(query).list();
            transaction.commit();
            session.close();
        } catch (Exception e) {
            transaction.rollback();
            session.close();
            e.printStackTrace();
        }

        return entities;

    }

}
