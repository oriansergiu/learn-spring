package com.internshipproject.exception;

import org.springframework.http.HttpStatus;

public abstract class InternshipException extends RuntimeException {

    private HttpStatus status = HttpStatus.BAD_REQUEST;

    private String messageKey;
    private String[] arguments;

    public InternshipException(String messageKey) {
        super(messageKey);
        this.messageKey = messageKey;
    }

    public InternshipException(String messageKey, String... arguments) {
        this.messageKey = messageKey;
        this.arguments = arguments;
    }

    public InternshipException(String messageKey, HttpStatus status, String... arguments) {
        this.messageKey = messageKey;
        this.status = status;
        this.arguments = arguments;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public String[] getArguments() {
        return arguments;
    }

    public void setArguments(String[] arguments) {
        this.arguments = arguments;
    }
}