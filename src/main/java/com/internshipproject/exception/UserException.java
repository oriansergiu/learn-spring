package com.internshipproject.exception;

import org.springframework.http.HttpStatus;

public class UserException extends InternshipException {

    public UserException(String messageKey, HttpStatus status, String... arguments) {
        super(messageKey, status, arguments);
    }
}