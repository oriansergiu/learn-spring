package com.internshipproject.controller;

import com.internshipproject.model.User;
import com.internshipproject.service.UserService;
import java.nio.file.AccessDeniedException;
import javax.annotation.security.PermitAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class LoginController {

    @Autowired
    private UserService userService;

    @PermitAll
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<User> login(@RequestBody User user) throws AccessDeniedException {

        return new ResponseEntity<>(userService.login(user), HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<User> registration(@RequestBody User user) throws AccessDeniedException {
        return new ResponseEntity<>(userService.registration(user), HttpStatus.OK);
    }
}